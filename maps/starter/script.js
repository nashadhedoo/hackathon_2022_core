/// <reference path="../node_modules/@workadventure/iframe-api-typings/iframe_api.d.ts" />

let currentPopup = undefined;
const today = new Date();
const time = today.getHours() + ":" + today.getMinutes();

WA.room.onEnterZone('clock', () => {
    currentPopup =  WA.ui.openPopup("clockPopup","It's " + time,[]);
})

WA.ui.registerIconCommand(
    'inventory', 
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAC9ElEQVR4nO2bTZLTMBCF39MYiiVVZLgCK2pmOABbqmCGw8Kw5AqQDZeAFAegKm4WkmVZ1siK43/lq1Ti2Ind3W49yfqhiMDn+u7Bbh/2j+0frAgRYey4msqQpVL0+xuBUFwD2bR0TgwAzasKgBsFgXV/RYGgrwEkA9bXDu9uPk5iWF8O+8focV8T0jKAAEmA25OMbo9Ik/IKVD0lY8Gk3VISVFdQxfORzZkeAnhasUjsbj5NZ80IdGnC9gr1iUQDwGBlvy0iAdim6vu0NGB3ez+TKdPga8L2b3EHlwDMbcD0NIU9vwB4FZuKHt0gumqv/Sy8o4Pz5+fXs/7v9k4NAhWAo637nAxgFg0fMKYB3H4ASOpHe0NdBFo9PMOTms7nFpsouWcArAjSftNyQIKqwKu3H9JOQ0JExr1bCZwqkn9/fYcc/0HKI4Az2gGh8YRVwOZGdg0hv6bz2gFpGuCmvZuCT+0fgrGuaTKg2TraNKx81UXYKQIrLdO9aWlAJi1BAO7NVtWOXFzX1GObjgYgDxkQASCBhyHmIYT1EG5DBM3OHJrCYt8AeNVgFiIox8bovQ5A5XcG4wA+jgYAVNsPgFQiaHACsH3nAbRmr1ivMyj9Bmk0ehXg9JRmkQWhDLAiuO48YIL9EqwGq51lOchF5iKtk8abFAZAQAV1VUA9e4GXb94nX5Akfv/4cpqVA/P63WcA2vmqmy7GYf8NbhBsh4iIJEVQKZ0BZZn2+7GpbEjPzKbNhd0pApiOwhhlOb/TIfreDB0AAYASIt0a4HN995CUekulUe9JQgaEWKvzgPM0KBCgRwasnbpXWABBzgHwmoi5kEPbN0r2AbjMFY4eXXH1lkr2RSD7ACSuGapZ+lzirjK/u72nO6KcfQZcAjC3AVPjz2lqaYBPlybEoZ58RQBQZluZvkcx3RClqW5L/VQ5cNXrl/mWheMGwJ6kOhf81ab6JfDH7Iaia/H0NAsBTZBtj2xlUr3WdhIzQky8ElIaH0vgP57U7ZfloewdAAAAAElFTkSuQmCC', 
    async () => {WA.player.state.inventory
        await WA.ui.website.open({
            url: "http://maps.workadventure.localhost/starter/inventory.html",
            position: {
                vertical: "middle",
                horizontal: "middle",
            },
            size: {
                height: "50vh",
                width: "70vw",
            },
            allowApi: true,
        });
    }
);
WA.room.onLeaveZone('clock', closePopUp)

function closePopUp(){
    if (currentPopup !== undefined) {
        currentPopup.close();
        currentPopup = undefined;
    }
}


WA.room.onEnterZone('doc', async () => {

    const inv = WA.player.state.inventory;
    if (!inv) {
        WA.player.state.inventory = JSON.stringify([]);
    }
    if (JSON.parse(WA.player.state.inventory).find((i) => i.name === "doc")) return;

    const effect = `WA.ui.website.open({
        url: "https://www.ecam.fr/wp-content/uploads/sites/3/2016/06/Exemple-fichier-PDF-1.pdf",
        position: {
            vertical: "middle",
            horizontal: "middle",
        },
        size: {
            height: "50vh",
            width: "50vw",
        },
    })`


    WA.player.state.inventory = JSON.stringify([
        ...JSON.parse(WA.player.state.inventory),
        {
            name: "doc",
            image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA8CAYAAAApK5mGAAAACXBIWXMAAC4jAAAuIwF4pT92AAAE7mlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNy4xLWMwMDAgNzkuYjBmOGJlOSwgMjAyMS8xMi8wOC0xOToxMToyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIzLjIgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMi0wNi0wOVQxMDo0NTo1MCswMjowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjItMDYtMDlUMTY6MTg6MzErMDI6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMjItMDYtMDlUMTY6MTg6MzErMDI6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOmQyODYyNzU4LTFiNjctZjU0YS1hM2E0LTZkMmU1MTZmYTkwNCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpkMjg2Mjc1OC0xYjY3LWY1NGEtYTNhNC02ZDJlNTE2ZmE5MDQiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkMjg2Mjc1OC0xYjY3LWY1NGEtYTNhNC02ZDJlNTE2ZmE5MDQiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmQyODYyNzU4LTFiNjctZjU0YS1hM2E0LTZkMmU1MTZmYTkwNCIgc3RFdnQ6d2hlbj0iMjAyMi0wNi0wOVQxMDo0NTo1MCswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIzLjIgKFdpbmRvd3MpIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PkoW0JAAAAHGSURBVGiB7ZoxSgNBFEDfbha1ECKCGhGsrFS8gFewF+w9jKWtJ/AMIlbWKS3EwkKJURS1CEFj1mIjJJH56wyzzI+ZByFk/+6f/7KzMxMySZ7nCJwBO8CndJIDCZACe0DTZ+KsJL4NrPhscIx53wnTkviT7wbH+PCdsExo4pg6oUbF7d/5TpjkeX4A1BkdyZLBaxeYpbo72QRegP7QsQzoAqeDdyuSXB63t4Ar26QWPAOLhlgDaNsmTIGWEK/bJrTkwXC8C3y5JEwZvd3j1FySWmDqHVJNIlM3yoVEXJOZ0CrUpxgwrClby4ViDjgBOhTTxw8zwC1wZLpQq1AGHBpiHQQhrV1O4kYKTqKQSBTyjMszLNYcWmjJ4ZoFKRh6lNunWJz2/nh+H1gHLvg98WZAK7TQucM1G8CxKRi6y7kg/ei8n0QhkSiknSiknSiknSiknSiknSiknSiknSiknSiknSiknSiknSiknSiknX8pJEk5bU+pGLGmFFgV4m9+a/HCuxBbC729zJYeRV3Xg8/DdaXAa1KyK7gNLFdTmzOPCBt7y7550/avkIg1aelK3pg6IWkEDIVYU9m/4JfAJnrmoxole2C/AeJ/RzgFv2YnAAAAAElFTkSuQmCC",
            effect: effect
        }
    ]);
})