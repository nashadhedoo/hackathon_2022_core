import { writable } from "svelte/store";
import { Icon } from "../Api/Events/ui/IconRegisterEvent";

function createIconStore() {
    const { subscribe, update, set } = writable(Array<Icon>());

    set(Array<Icon>());

    return {
        subscribe,
        add: (icon: Icon) => {
            update((currentArray) => [...currentArray, icon]);
        },
        update: (icon: Icon) => {
            update((currentArray) =>
                currentArray.map((currentIcon) => (currentIcon.name === icon.name ? icon : currentIcon))
            );
        },
        remove: (icon: Icon) => {
            update((currentArray) => currentArray.filter((currentIcon) => currentIcon.name !== icon.name));
        },
    };
}

export const iconStore = createIconStore();
