import { get } from "svelte/store";
import { Icon, IconRegisterEvent } from "../../Api/Events/ui/IconRegisterEvent";
import { iconStore } from "../../Stores/IconStore";

class IconManager {
    constructor() {}

    public open(iconConfig: IconRegisterEvent): Icon {
        const icon: Icon = {
            ...iconConfig,
        };

        iconStore.add(icon);

        return icon;
    }

    public getAll(): Icon[] {
        return get(iconStore);
    }

    public close(iconName: string) {
        const icon = get(iconStore).find((currentIcon) => currentIcon.name === iconName);

        if (!icon) {
            return;
        }

        iconStore.remove(icon);
    }
}

export const iconManager = new IconManager();
