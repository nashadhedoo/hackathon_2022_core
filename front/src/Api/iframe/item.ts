
import { Subject } from "rxjs";
import { IframeApiContribution, queryWorkadventure, sendToWorkadventure } from "./IframeApiContribution";
import { apiCallback } from "./registeredCallbacks";

const item: Map<string, Item> = new Map<string, Item>();

interface ItemInterface {
    name : string
    effect : Function
}
export class Item{
    constructor(public name : string, public effect: Function){};
}
export class WorkadventureItemCommands extends IframeApiContribution<WorkadventureItemCommands> {
    callbacks = [
        // apiCallback({
        //     callback: (payloadData: EnterLeaveEvent) => {
        //         enterStreams.get(payloadData.name)?.next();
        //     },
        //     type: "enterEvent",
        // })
    ];

    OnGetItem(itemName: string, itemFunction: Function): void {
        console.log('onGetItem');
        let subject = item.get(itemName);
        if (subject === undefined) {
            subject = new Item(itemName, itemFunction);
        }
        let inventory: {[key: string]: ItemInterface} | string | null = localStorage.getItem('inventory');
        if (!inventory) inventory = {};
        else if(typeof inventory === "string") inventory = JSON.parse(inventory) as {[key: string]: ItemInterface};
        inventory[`${itemName}`] = subject;
        localStorage.setItem("inventory", JSON.stringify({...inventory}));
    }
}
export default new WorkadventureItemCommands();