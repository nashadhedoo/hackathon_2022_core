import type { IconClickedEvent } from "../../Events/ui/IconClickedEvent";
import { iframeListener } from "../../IframeListener";

export function sendIconClickedEvent(iconName: string) {
    iframeListener.postMessage({
        type: "iconClicked",
        data: {
            iconName,
        } as IconClickedEvent,
    });
}
