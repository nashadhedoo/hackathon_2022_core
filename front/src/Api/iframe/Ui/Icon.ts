import { sendToWorkadventure } from "../IframeApiContribution";

export class Icon {
    constructor(private iconName: string, private iconImage: string) {}

    /**
     * remove the icon
     */
    public remove() {
        sendToWorkadventure({
            type: "unregisterIcon",
            data: {
                name: this.iconName,
                image: this.iconImage,
            },
        });
    }
}
