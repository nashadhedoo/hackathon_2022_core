import { z } from "zod";

/**
 * A message sent from a script to the game to remove a custom icon from the icon
 */
export const isIconUnregisterEvent = z.object({
    name: z.string(),
    image: z.string(),
});

export type UnregisterIconEvent = z.infer<typeof isIconUnregisterEvent>;

/**
 * A message sent from a script to the game to add a custom icon from the icon
 */
export const isIconRegisterEvent = z.object({
    name: z.string(),
    image: z.string()
});

export type IconRegisterEvent = z.infer<typeof isIconRegisterEvent>;

export const isIcon = z.object({
    name: z.string(),
    image: z.string(),
});

export type Icon = z.infer<typeof isIcon>;
