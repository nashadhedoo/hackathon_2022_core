import { z } from "zod";

export const isIconClickedEvent = z.object({
    iconName: z.string(),
});

/**
 * A message sent from the game to the iFrame when a menu item is clicked.
 */
export type IconClickedEvent = z.infer<typeof isIconClickedEvent>;
